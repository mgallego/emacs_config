# Emacs Config

This is my emacs configuration.

This version uses literal programming with org-babel.

It is heavely influenced by [Arjen Wiersma's configuration](https://gitlab.com/buildfunthings/emacs-config/blob/master/init.el)

Go ahead, read the entire configuration by opening [config.org](config.org).

## Dependencies

Install intelephense as PHP LSP

```
sudo npm i -g intelephense
```

## Setup

```
~ $ git clone git@gitlab.com:mgallego/emacs_config.git .emacs.d
~/.emacs.d $ git submodule init
~/.emacs.d $ git submodule update
```

When you start GNU Emacs it will start downloading all the required packages. It might take some time, depending on your internet connection and machine. Be sure to first perform the manual steps listed in [config.org](config.org).

Afterwards, restart GNU Emacs and it will load up very speedy.


## Run emacs as Daemon/Client

To run an emacs instance as a client from a deamon execute the next command

`emacsclient -a "" -c`

-a "" Create a new daemon if doesn't exists
-c Create a new frame


Add as a system key the next command: emacsclient -ne "(make-capture-frame)"
