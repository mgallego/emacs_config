;; init.el --- Configuration of Emacs

;;
;; (c) 2017 Moises Gallego
;;

;; Based on: https://gitlab.com/buildfunthings/emacs-config/blob/master/init.el

(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")
(package-initialize)

(require 'org)
(require 'ob-tangle)

;; Load the literal configuration
(setq init-dir (file-name-directory (or load-file-name (buffer-file-name))))
;; (org-babel-load-file (expand-file-name "config.org" init-dir))
(mapc #'org-babel-load-file (directory-files init-dir t "^config.org$"))


;; (custom-set-faces
;;  ;; custom-set-faces was added by Custom.
;;  ;; If you edit it by hand, you could mess it up, so be careful.
;;  ;; Your init file should contain only one such instance.
;;  ;; If there is more than one, they won't work right.
;;  )
;; (put 'narrow-to-region 'disabled nil)
